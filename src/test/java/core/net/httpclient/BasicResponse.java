package core.net.httpclient;

import io.restassured.response.Response;

public class BasicResponse implements CustomResponse
{
    private final int statusCode;
    private final String jsonResponse;
    private final Headers headersDTO;

    public BasicResponse(Response response)
    {
        this.statusCode = response.statusCode();
        this.jsonResponse = response.asString();
        this.headersDTO = matchHeaders(response);
    }

    public int getStatusCode()
    {
        return statusCode;
    }

    public String getJsonResponse()
    {
        return jsonResponse;
    }

    public Headers getHeaders() { return headersDTO; }

    private Headers matchHeaders(Response response)
    {
        Headers headers
                = new BasicHeaders(
                response.header("Server"),
                response.header("Content-Type"),
                response.header("Connection"),
                response.header("X-XSS-Protection"),
                response.header("Strict-Transport-Security"),
                response.header("Cache-Control"));

        return headers;
    }
}
