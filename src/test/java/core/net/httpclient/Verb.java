package core.net.httpclient;

public enum Verb {
    GET,
    POST,
    DELETE,
    PATCH
}
