package core.net.httpclient;

public interface CustomResponse
{
    int getStatusCode();
    String getJsonResponse();
    Headers getHeaders();
}
