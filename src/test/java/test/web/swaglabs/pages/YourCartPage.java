package test.web.swaglabs.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import test.web.swaglabs.pages.CheckoutPages.CheckoutYourInformationPage;

public class YourCartPage extends BasePage
{
    public YourCartPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "checkout")
    WebElement checkout;

    public CheckoutYourInformationPage clickCheckout()
    {
        checkout.click();
        return new CheckoutYourInformationPage(driver);
    }
}
