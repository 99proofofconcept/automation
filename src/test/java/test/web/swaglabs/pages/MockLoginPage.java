package test.web.swaglabs.pages;

import org.openqa.selenium.WebDriver;

public class MockLoginPage
{
    private final WebDriver driver;

    public MockLoginPage(WebDriver driver)
    {
        this.driver = driver;
    }

    /*
      This method should mock the authentication to avoid the real login
      However in this case is not possible.
     */
    public ProductsPage giveMeAuthorization()
    {
        driver.navigate().to("https://www.saucedemo.com/");

        LoginPage loginPage = new LoginPage(driver);

        return loginPage
                .writeUser("standard_user")
                .writePassword("secret_sauce")
                .doLogin();
    }
}
