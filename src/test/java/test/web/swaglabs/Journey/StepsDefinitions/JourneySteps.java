package test.web.swaglabs.Journey.StepsDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import test.web.swaglabs.Journey.TestContext;
import test.web.swaglabs.pages.CheckoutPages.CheckoutCompletePage;
import test.web.swaglabs.pages.CheckoutPages.CheckoutOverviewPage;
import test.web.swaglabs.pages.CheckoutPages.CheckoutYourInformationPage;
import test.web.swaglabs.pages.MockLoginPage;
import test.web.swaglabs.pages.ProductsPage;
import test.web.swaglabs.pages.YourCartPage;

public class JourneySteps
{
    private TestContext testContext;

    @Before
    public void setup() {
        this.testContext = new TestContext();
    }

    @Given("I login")
    public void i_login()
    {
        MockLoginPage mockLoginPage = new MockLoginPage(testContext.getWebDriver().getDriver());
        ProductsPage productsPage = mockLoginPage.giveMeAuthorization();

        testContext.getScenarioContext().setContext("productsPage", productsPage);
    }

    @Given("select a couple of products")
    public void select_a_couple_of_products()
    {
        ProductsPage productsPage =
                (ProductsPage) testContext.getScenarioContext().getContext("productsPage");

        YourCartPage yourCartPage = productsPage
                    .addSauceLabsProduct()
                    .addSauceLabsBikeLight()
                    .clickShoppingButton();

         testContext.getScenarioContext().setContext("yourCartPage", yourCartPage);
    }


    @Given("do checkout")
    public void do_checkout()
    {
        YourCartPage yourCartPage = (YourCartPage) testContext.getScenarioContext().getContext("yourCartPage") ;

        CheckoutYourInformationPage checkoutYourInformationPage = yourCartPage.clickCheckout();

        testContext.getScenarioContext().setContext("checkoutYourInformationPage", checkoutYourInformationPage);
    }


    @Given("fill the information out")
    public void fill_the_information_out()
    {
        CheckoutYourInformationPage checkoutYourInformationPage =
                (CheckoutYourInformationPage) testContext.getScenarioContext().getContext("checkoutYourInformationPage");

        CheckoutOverviewPage checkoutOverviewPage = checkoutYourInformationPage
                .fillFirstName("Test")
                .fillLastName("Journey")
                .fillPostalCode("0001")
                .doContinue();

        testContext.getScenarioContext().setContext("checkoutOverviewPage", checkoutOverviewPage);

    }


    @When("I finish the purchase")
    public void i_finish_the_purchase()
    {
        CheckoutOverviewPage checkoutOverviewPage =
                (CheckoutOverviewPage) testContext.getScenarioContext().getContext("checkoutOverviewPage");

        CheckoutCompletePage checkoutCompletePage = checkoutOverviewPage.clickFinish();

        testContext.getScenarioContext().setContext("checkoutCompletePage", checkoutCompletePage);
    }


    @Then("the checkout has been completed")
    public void the_checkout_has_been_completed()
    {
        CheckoutCompletePage checkoutCompletePage =
                (CheckoutCompletePage) testContext.getScenarioContext().getContext("checkoutCompletePage");

        Assert.assertTrue(checkoutCompletePage.isCheckoutCompletePageVisible());
    }

    @After
    public void teardown()
    {
        testContext.dispose();
    }
}
