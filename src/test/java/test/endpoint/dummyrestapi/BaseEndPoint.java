package test.endpoint.dummyrestapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.net.httpclient.CustomResponse;

public class BaseEndPoint
{
    protected final String baseUrl;
    protected CustomResponse basicResponse;
    protected ObjectMapper objectMapper;
    protected JsonNode jsonNode;

    public BaseEndPoint(String baseUrl)
    {
        this.baseUrl = baseUrl;
    }

    public int getResponseStatusCode()
    {
        return (basicResponse != null) ? basicResponse.getStatusCode() : -1;
    }

    protected void extractJsonNodeByName(String nameNode) throws JsonProcessingException
    {
        objectMapper = new ObjectMapper();
        jsonNode = objectMapper.readTree(basicResponse.getJsonResponse()).get(nameNode);
    }
}
