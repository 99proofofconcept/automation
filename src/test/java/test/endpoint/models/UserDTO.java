package test.endpoint.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserDTO
{
     private int id;
     private String name;
     private String email;
     private String gender;
     private String status;
}
