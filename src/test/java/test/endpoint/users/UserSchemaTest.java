package test.endpoint.users;

import core.net.httpclient.CustomResponse;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.endpoint.BaseTest;
import test.endpoint.dummyrestapi.UsersEndPoint;

public class UserSchemaTest extends BaseTest
{
    private UsersEndPoint usersEndPoint;
    private String baseUrl = "https://gorest.co.in";


    @BeforeClass
    public void setUp()
    {
        usersEndPoint = new UsersEndPoint(baseUrl, token);
    }

    @Test
    public void user_json_and_user_spec_should_match()
    {
        String userId = "1532";
        String schemaUserSpec = "/schemas/UserSpec.json";

        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(UserSchemaTest.class.getResourceAsStream(schemaUserSpec)));

        CustomResponse customResponse = usersEndPoint.getJsonResponseFromUserById(userId);

        JSONObject jsonSubject = new JSONObject(new JSONTokener(customResponse.getJsonResponse()));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }

    @AfterClass
    public void tearDown()
    {
        usersEndPoint.Dispose();
        usersEndPoint = null;
    }
}
