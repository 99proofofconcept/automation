package test.endpoint;

import org.testng.annotations.BeforeClass;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BaseTest
{
    protected String token;

    @BeforeClass
    protected void readConfiguration() throws IOException
    {
        String propFileName = "settings.config";
        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null)
        {
            properties.load(inputStream);
        }
        else
        {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        token = properties.getProperty("dummyrestapi.token");
    }
}
